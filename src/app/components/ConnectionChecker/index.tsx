import * as React from 'react';
import { connect } from 'react-redux';
import { iRootState, Dispatch } from '../../../store';
import { ConnectionTitle, AlignCenterField  } from '../../styled'; 

const mapState = (state: iRootState) => ({
	connect: state.connect,
});

const mapDispatch = (dispatch: Dispatch) => ({
	next: dispatch.connect.next,
});

class ConnectionChecker extends React.Component<any> {
  render () {
    const { connect, next } = this.props;

    return (
      <AlignCenterField> 
         <ConnectionTitle state={connect.state}>{connect.state}</ConnectionTitle>
      </AlignCenterField>
    );
  }
}

export default connect(mapState, mapDispatch)(ConnectionChecker);