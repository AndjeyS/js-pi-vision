import * as React from 'react';
import { connect } from 'react-redux';
import { iRootState, Dispatch } from '../../../store';

const mapState = (state: iRootState) => ({
	test: state,
});

const mapDispatch = (dispatch: Dispatch) => ({
	next: dispatch.connect.next,
});

class EmojiText extends React.Component<any> {
  render () {
    return (
      <div> 
        <h3>
          App work!
          <span role="img" aria-label="Loveman">😍</span>
        </h3>
        <button onClick={() => this.props.next('connect')}>Next state</button>
        <button onClick={() => this.props.next('disconect')}>Next state</button>
      </div>
    );
  }
}

export default connect(mapState, mapDispatch)(EmojiText);