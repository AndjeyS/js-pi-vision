import * as React from 'react';
import { Provider } from 'react-redux';
import { store } from '../store';
import { ConnectionChecker } from './components';

class App extends React.Component {
  render () {
    return (
      <Provider store={store}>
        <ConnectionChecker />
      </Provider>
    );
  }
}

export default App;
