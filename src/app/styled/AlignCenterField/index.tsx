import * as React from 'react';
import styled from 'styled-components'

const AlignCenterField = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export default AlignCenterField;
