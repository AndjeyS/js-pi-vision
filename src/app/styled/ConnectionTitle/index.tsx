import * as React from 'react';
import styled from 'styled-components'

const ConnectionTitle = styled.h1`
  display: flex;
  justify-content: center;
  color: ${(props: any) => props.state === 'connect' ? 'green' : 'tomato'};
`;

export default ConnectionTitle;

