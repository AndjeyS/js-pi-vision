import Machine from '@qiwi/cyclone';

const machine = new Machine({
  initialState: 'disconect',
  initialData: {},  
  transitions: {
    'disconect>connect': true,
    'connect>disconect': true,
  }
});

export default machine;
