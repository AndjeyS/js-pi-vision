import { createModel } from '@rematch/core';
import machine from './machine';

const model = createModel({
  state: machine.current(),
  reducers: {
    next: (prev:any, next: string, ...payload: any[]): any => {
      return machine.next(next, ...payload).current();
    }
  }
});

export default model;
